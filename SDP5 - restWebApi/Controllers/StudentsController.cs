﻿using Newtonsoft.Json;
using SDP5___restWebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace SDP5___restWebApi.Controllers
{
    [RoutePrefix("api/Students")]
    public class StudentsController : ApiController
    {
        private List<Student> students = new StudentsCollection();
        [Route("")]
        [HttpGet]
        // GET: api/Students
        public IHttpActionResult Get()
        {
            return Ok(students);
        }

        // GET: api/Students/5
        [Route("{id:int}")]
        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            Student element = new Student();
            try
            {
                element = students.ElementAt(id);
                return Ok(element);
            }
            catch (ArgumentOutOfRangeException outOfRange)
            {
                return NotFound();
            }
        }

        //[Route("{name}")]
        //public IHttpActionResult Get(string name)
        //{
        //    var  student= students.FirstOrDefault(s => s.FirstName.Contains(name));

        //    if(student == null)
        //    {
        //        return NotFound();
        //    }else
        //    {
        //        return Ok(student);
        //    }
        //}

        [Route("{name}")]
        [HttpGet]
        public IHttpActionResult FindStudentByName(string name)
        {
            var student = students.FirstOrDefault(s => s.FirstName.Contains(name));

            if (student == null)
            {
                return NotFound();
            }
            else
            {
                return Ok(student);
            }
        }

        // POST: api/Students
        [Route("")]
        [HttpPost]
        public IHttpActionResult Post([FromBody]Student newStudent)
        {
            newStudent.Id = students.Count();
            students.Add(newStudent);
            return Ok(students);
        }

        // PUT: api/Students/5
        [Route("{id:int}")]
        [HttpPut]
        public IEnumerable<Student> Put(int id, [FromBody]Student updatedStudent)
        {
            var student = students.FirstOrDefault(s => s.Id == id);
            if (student != null)
            {
                student.FirstName = updatedStudent.FirstName;
                student.Lastname = updatedStudent.Lastname;
            }
            return students;
        }

        // DELETE: api/Students/5
        [Route("{id:int}")]
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var student = students.FirstOrDefault(s => s.Id == id);
            if (student != null)
            {
                students.Remove(student);
                return Ok(students);
            }
            else
            {
                return NotFound();
            }
        }
    }
}
