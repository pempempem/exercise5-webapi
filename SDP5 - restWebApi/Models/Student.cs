﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SDP5___restWebApi.Models
{
    public class Student
    {
        public int Id
        {
            get; set;
        }
        public string FirstName
        {
            get; set;
        }
        public string Lastname
        {
            get; set;
        }
    }

    public class StudentsCollection : List<Student> {
        public StudentsCollection() {
            var students = new Student[] {
            new Student { Id = 0, FirstName = " Roch", Lastname = "Kowalski" },
            new Student { Id = 1, FirstName = " Anna", Lastname = "Kowalski" },
            new Student { Id = 2, FirstName = " Martyna", Lastname = "Kowalski" },
            new Student { Id = 3, FirstName = " Bazyli", Lastname = "Kowalski" }
            };
            this.AddRange(students);
        }
    }
}